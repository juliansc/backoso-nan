var express = require('express');
var router = express.Router();
var http = require('http');
const request = require('request-promise');
const cheerio = require('cheerio');

/* Base Module URL*/
router.get('/', (req, res) => res.send('poner parametro en la url, /url?url=http://ejemplo.com'));

/* If url got data */
router.get('/url', function (req, res, next) {
  //Run a method with async
  (async () => {

      let webData = [];
      let web = req.query.url;

      const response = await request({
        uri: web,
        headers: {
          "content-type": "text/html; charset=utf-8",
          "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
          "accept-encoding": "gzip, deflate",
          "accept-language": "es-ES,es;q=0.9"
        },
        gzip: true
      });

      //load cheerio to get items of the response
      let $ = cheerio.load(response);

      //get items with cheerio and jquery
      let title = web.substring(
        web.lastIndexOf("//") + 2,
        web.lastIndexOf("")
      );

      var dependencies = $("[src*='.js']").map(function () {

        var str = $(this).attr("src");

        //here filter all the dependencies with .js
        var dependency = str.substring(
          str.lastIndexOf("/") + 1,
          str.lastIndexOf(".js") + 3
        );

        return dependency;
      }).get();

      var bytes = req.socket.bytesRead;

      //data saved on a json response
      webData.push({
        title,
        dependencies,
        bytes
      });

      //data served to front
      res.json(webData[0]);
    }

  )();




});

module.exports = router;